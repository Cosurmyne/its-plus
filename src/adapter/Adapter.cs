using root.adapter;

namespace root
{
    class Adapter : ITarget
    {
        private Marks _adaptee;
	public byte Id { get;set; }
	public MarksIn Template { get;set; }

        public Adapter(Marks adaptee)
        {
            _adaptee = adaptee;
        }

        public void DoStuff()
        {
		_adaptee.Capture(Id, Template);
        }

        public void Renege()
        {
		_adaptee.Clear(Id);
        }
    }
}
