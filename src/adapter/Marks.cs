namespace root.adapter
{
    class Marks
    {
        public void Capture(byte id, MarksIn template)
        {
            CaptureMarks command = new CaptureMarks(root.ITS.GetITS().TM[id - 1]);
	    command.Template = template;
            App invoker = new App(command);
            invoker.PushForward();
        }
        public void Clear(byte id)
        {
            CaptureMarks command = new CaptureMarks(root.ITS.GetITS().TM[id - 1]);
            App invoker = new App(command);
            invoker.RollBack();
        }
    }
}
