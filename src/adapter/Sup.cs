namespace root.adapter
{
    class Sup : ITarget
    {
        public void DoStuff()
        {
            CaptureMarks command = new CaptureMarks(root.ITS.GetITS().TM[5 - 1]);
            command.Template = new TM5();
            App invoker = new App(command);
            invoker.PushForward();
        }

        public void Renege()
        {
            CaptureMarks command = new CaptureMarks(root.ITS.GetITS().TM[5 - 1]);
            App invoker = new App(command);
            invoker.RollBack();
            System.Console.Write("Marks Cleared");
            System.Console.ReadLine();
        }
    }
}
