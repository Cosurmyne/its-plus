namespace root
{
    public sealed class Student
    {
        private static Student _instance;
        private static readonly object _lockThis = new object();

        private string _STDNo;
        private string _fName;
        private string _lName;

        public string STDNum
        {
            get { return _STDNo; }
            set
            {
                _STDNo = value != null && value.Trim().Length > 0 ? value.Trim() : null;
            }
        }
        public string FName
        {
            get { return _fName; }
            set
            {
                _fName = value != null && value.Trim().Length > 0 ? value.Trim() : null;
            }
        }
        public string LName
        {
            get { return _lName; }
            set
            {
                _lName = value != null && value.Trim().Length > 0 ? value.Trim() : null;
            }
        }

        private Student()
        {
            Reset();
        }

        public static Student GetStudent()
        {
            lock (_lockThis)
            {
                if (_instance == null) _instance = new Student();
            }
            return _instance;
        }

        public void Reset()
        {
            STDNum = null;
            FName = null;
            LName = null;
        }

        public bool NameReady()
        {
            return STDNum != null || (FName != null && LName != null);
        }

        public override string ToString()
        {
            if (!NameReady()) return "John Doe";
            return STDNum != null ? FName + " " + LName + " (" + STDNum + ")" : FName + " " + LName;
        }
    }
}
