namespace root
{
    public sealed class ITS
    {
        private static ITS _instance;
        private static readonly object _lockThis = new object();

        private readonly Database[] _tm = new Database[5];

        public Database[] TM { get { return _tm; } }

        private ITS()
        {
            _tm[0] = new Database(1);
            _tm[1] = new Database(2);
            _tm[2] = new Database(3);
            _tm[3] = new Database(4);
            _tm[4] = new Database(5);
        }

        public static ITS GetITS()
        {
            lock (_lockThis)
            {
                if (_instance == null) _instance = new ITS();
            }
            return _instance;
        }
    }
}
