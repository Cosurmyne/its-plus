namespace root
{
	class CaptureStudent : ICommand
	{
		protected UI _receiver;

		public CaptureStudent(UI receiver)
		{
			_receiver = receiver;
		}
		public void Execute()
		{
			_receiver.CaptureStudent();
		}
		public void Undo()
		{
			_receiver.ClearStudent();
		}
	}
}
