namespace root
{
	class CaptureMarks : ICommand
	{
		protected Database _receiver;
		public MarksIn Template { get; set; }

		public CaptureMarks(Database receiver)
		{
			_receiver = receiver;
		}
		public void Execute()
		{
			_receiver.CaptureMarks(Template);
		}
		public void Undo()
		{
			_receiver.ClearMarks();
		}
	}
}
