namespace root
{
	public class ViewITS : ICommand
	{
		protected UI _receiver;

		public ViewITS(UI receiver)
		{
			_receiver = receiver;
		}
		public void Execute()
		{
			_receiver.ViewITS();
		}
		public void Undo()
		{
			_receiver.ClearITS();
		}
	}
}
