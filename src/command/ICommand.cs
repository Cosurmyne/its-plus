namespace root
{
	public interface ICommand
	{
		void Execute();
		void Undo();
	}
}
