using System;

namespace root
{
    public class Supped : Preview
    {
        public override void Sup()
        {
            Console.WriteLine("{0}: {1}", "Sup Mark".PadRight(13), ITS.GetITS().TM[4].Marks);
            Console.WriteLine("{0}: {1}", "New Total".PadRight(13), Marks());
        }
        public override byte? Marks()
        {
            Database[] TM = ITS.GetITS().TM;
            double? total = .0;
            total += TM[0].Marks == null ? 0 : TM[0].Marks * .3;
            total += TM[1].Marks == null ? 0 : TM[1].Marks * .3;
            total += TM[2].Marks == null ? 0 : TM[2].Marks * .3;
            total += TM[3].Marks == null ? 0 : TM[3].Marks * .1;
            total += TM[4].Marks != null ? TM[4].Marks * .1 : 0;
            return total == null ? null : (byte?)decimal.Round((decimal)total);
        }
    }
}
