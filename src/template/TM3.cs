using System;

namespace root
{
    public class TM3 : MarksIn
    {
        public override void Prompt(ref byte? target)
        {
            Int32 val = new Int32();
            do
            {
                Console.Clear();
                Console.WriteLine("Test Prompt");
                Console.WriteLine("===========");
                Console.WriteLine();
                Console.Write("Mark (%) >> ");
            } while (!Utensil.IsInt(Console.ReadLine(), ref val) || val > 100);
	    target = (byte) val;
        }
    }
}
