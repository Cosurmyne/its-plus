using System;

namespace root
{
    public abstract class Preview
    {
        public void Print()
        {
            Console.Clear();
            if (Student.GetStudent().NameReady()) TargetStudent();
            MarkSheet();
            if (new TM5().Complete()) MarkTotal();
            Sup();
        }

        public virtual void Sup() { }
        public virtual byte? Marks()
        {
            Database[] TM = ITS.GetITS().TM;
            double? total = .0;
            total += TM[0].Marks == null ? 0 : TM[0].Marks * .3;
            total += TM[1].Marks == null ? 0 : TM[1].Marks * .3;
            total += TM[2].Marks == null ? 0 : TM[2].Marks * .3;
            total += TM[3].Marks == null ? 0 : TM[3].Marks * .1;
            return total == null ? null : (byte?)decimal.Round((decimal)total);
        }

        private void TargetStudent()
        {
            Console.WriteLine(Student.GetStudent().ToString());
            Console.WriteLine();
        }
        private void MarkTotal()
        {
            Database[] TM = ITS.GetITS().TM;
            byte? mark = Marks();
            Console.WriteLine("Total Mark: {0}".PadRight(13), mark);
            Console.WriteLine("Archievement: {0}".PadRight(13), Archievement(mark));
            Console.WriteLine();
        }
        private void MarkSheet()
        {
            Database[] TM = ITS.GetITS().TM;
            Console.WriteLine("----------------------------------");
            Console.WriteLine("| FIELD  | TM1 | TM2 | TM3 | TM4 |");
            Console.WriteLine("----------------------------------");
            Console.WriteLine("| Mark   | {0} | {1} | {2} | {3} |", TM[0].Marks.ToString().PadRight(3), TM[1].Marks.ToString().PadRight(3), TM[2].Marks.ToString().PadRight(3), TM[3].Marks.ToString().PadRight(3));
            Console.WriteLine("| Result | {0}   | {1}   | {2}   | {3}   |", TM[0].Symbol(), TM[1].Symbol(), TM[2].Symbol(), TM[3].Symbol());
            Console.WriteLine("| DP     | {0} | {1} | {2} | {3} |", DP(TM[0].Marks, 1).ToString().PadRight(3), DP(TM[1].Marks, 2).ToString().PadRight(3), DP(TM[2].Marks, 3).ToString().PadRight(3), DP(TM[3].Marks, 4).ToString().PadRight(3));
            Console.WriteLine("----------------------------------");
            Console.WriteLine();
        }
        private byte? DP(byte? mark, byte TM)
        {
            double[] weight = { .3, .3, .3, .1, .1 };
            return mark == null ? null : (byte?)decimal.Round((decimal)(mark * weight[TM - 1]));
        }
        private string Archievement(byte? mark)
        {
            if (mark >= 75) return "Distinction";
            if (mark >= 50) return "Pass";
            if (mark >= 45) return "Extra Sup";
            return "Fail";
        }
    }
}
