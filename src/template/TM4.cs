using System;

namespace root
{
    public class TM4 : MarksIn
    {
        public override void Prompt(ref byte? target)
        {
            Int32 val = new Int32();
            do
            {
                Console.Clear();
                Console.WriteLine("Assignment Prompt");
                Console.WriteLine("=================");
                Console.WriteLine();
                Console.Write("Mark (%) >> ");
            } while (!Utensil.IsInt(Console.ReadLine(), ref val) || val > 100);
	    target = (byte) val;
        }
    }
}
