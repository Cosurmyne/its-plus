using System;

namespace root
{
    public class TM5 : MarksIn
    {
        public override void Prompt(ref byte? target)
        {
            Int32 val = new Int32();
            do
            {
                Console.Clear();
                Console.WriteLine("Supplementary Prompt");
                Console.WriteLine("====================");
                Console.WriteLine();
                Console.Write("Mark (%) >> ");
            } while (!Utensil.IsInt(Console.ReadLine(), ref val) || val > 100);
            target = (byte)val;
        }
        public override bool CheckSup()
        {
            bool qualify = true;
            if (!Complete()) qualify = false;
            byte? mark = new Supped().Marks();
            if (mark < 45 || mark > 49) qualify = false;

            if (!qualify)
            {
                Console.WriteLine("Student doesn't qualify for Sup");
                Console.ReadLine();
            }

            return qualify;
        }
        public bool Complete()
        {
            Database[] TM = ITS.GetITS().TM;
            bool good = true;
            if (TM[0].Marks == null) return false;
            if (TM[1].Marks == null) return false;
            if (TM[2].Marks == null) return false;
            if (TM[3].Marks == null) return false;
            return good;
        }
    }
}
