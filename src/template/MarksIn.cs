namespace root
{
	public abstract class MarksIn
	{
		public void Capture(ref byte? target)
		{
			if(CheckSup()) Prompt(ref target);
		}
		public abstract void Prompt(ref byte? target);
		public virtual bool CheckSup(){ return true; }
	}
}
