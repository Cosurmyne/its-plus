using System;
namespace root
{
    public class Database
    {
        public byte? _marks;
        private string _dbms;

        public byte? Marks { get{ return _marks; } }
        public string DBMS { get{ return _dbms; } }

        public Database(byte id)
        {
            switch (id)
            {
                case 1: _dbms = "MySQL"; break;
                case 2: _dbms = "SQLite"; break;
                case 3: _dbms = "Firebase"; break;
                case 4: _dbms = "MSSQL"; break;
                case 5: _dbms = "MSAccess"; break;
                default: throw new Exception("DBMS not known");
            }
        }
	public void CaptureMarks(MarksIn template)
	{	
		template.Capture(ref _marks);
	}

        public void ClearMarks()
        {
            _marks = null;
        }
        public char Symbol()
        {
            if (Marks == null) return '-';
            if (Marks >= 75) return 'D';
            if (Marks >= 50) return 'P';
            return 'F';
        }
    }
}
