using System;

namespace root
{
    public class UI
    {
        public void ViewITS()
        {
            Preview[] selection = {new Primitive(), new Supped()};
	    Preview display = selection[ITS.GetITS().TM[4].Marks == null ? 0 : 1];
	    display.Print();
            Console.ReadLine();
        }
        public void ClearITS()
        {
            foreach (Database db in ITS.GetITS().TM)
            {
                db.ClearMarks();
            }
            Console.Write("Marks Cleared");
            Console.ReadLine();
        }

        public void CaptureStudent()
        {
            Student focus = Student.GetStudent();
            Console.Write("Student Number".PadRight(20) + ": ");
            focus.STDNum = Console.ReadLine();
            Console.Write("First Name".PadRight(20) + ": ");
            focus.FName = Console.ReadLine();
            Console.Write("Last Name".PadRight(20) + ": ");
            focus.LName = Console.ReadLine();

            Console.WriteLine();
            Console.Write("Details Saved");
            Console.ReadLine();
        }
        public void ClearStudent()
        {
            Student.GetStudent().Reset();
            Console.Write("Details Cleared");
            Console.ReadLine();
        }
    }
}
